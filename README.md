# STWM Sauna - Kalender

Die Saunen der Münchner Münchener Stadtwerke bietet im Winter besondere Saunaevents an. Dieses Skript liefert diese Saunaevents als .ics Kalender zum einbinden in andere Programme/Apps. Folgende Kalender befinden sich im Repo:

* Winter 2018/2019
* Winter 2019/2020

## Benutzung

./saunacal NameDerAusgabeDatei.ics

# STWM Sauna - Calendar

The saunas of the Stadtwerke Munich offer special sauna events in winter. This script assemblies a .ics calendar containing those sauna events. The following seasons are included in the repo.

* winter 2018/2019
* winter 2019/2020

## Usage

./saunacal OuputFile.ics
